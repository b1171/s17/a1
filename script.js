let studentList = [];

function addStudent(student) {
	studentList.push(student);
	console.log(`${student} was added to the student's list.`)
}

addStudent("John");
addStudent("Jane");
addStudent("Joe");

function countStudents(array){
	console.log(`There are a total of ${array.length} students enrolled.`);
}

countStudents(studentList);

function printStudents(array){
	let newArray = array.sort(
		function(a,b){
			return (a - b); 
		}
	);

	newArray.forEach(
			function(x) {
				console.log(x);
			}
		)
}

printStudents(studentList);

function findStudent(name,array){
	let lowerCaseArray = array.map (
			function(x) {
				return x.toLowerCase();
			}
		)

	let newArray = lowerCaseArray.filter(
			function(x) {
				return x.includes(name.toLowerCase());
			}
		);

	let message;

	if(newArray.length == 0) {
		message = `${name} is not an enrollee`;
	}else if(newArray.length == 1) {
		message = `${name} is an enrollee`;
	}else if(newArray.length > 1) {
		message = newArray.toString() + " are enrollees.";
	}

	console.log(message);
}

findStudent("joe",studentList);
findStudent("bill",studentList);
findStudent("j",studentList);

function addSection(section,array) {
	array = array.map (
		function(x) {
			return x + ` - Section ${section}`;
		}
	);

	console.log(array);
}

addSection("A",studentList);

function removeStudent(name) {
	let titleCaseName = name.slice(0,1).toUpperCase + name.slice(1);
	studentList.splice(studentList.indexOf(titleCaseName), 1);
	console.log(`${name} was removed from the student's list.`)
}

removeStudent("joe");
console.log(studentList);

